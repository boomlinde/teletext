import sys

def parity(b):
    orig = b
    set = 0
    while b:
        set += b & 1
        b >>= 1
    odd_parity = ((set & 1) << 7)
    if not odd_parity:
        orig |= 0x80
    return orig


def ham(d):
    d1 = d&1
    d2 = (d>>1)&1
    d3 = (d>>2)&1
    d4 = (d>>3)&1

    p1 = (1 + d1 + d3 + d4) & 1
    p2 = (1 + d1 + d2 + d4) & 1
    p3 = (1 + d1 + d2 + d3) & 1
    p4 = (1 + p1 + d1 + p2 + d2 + p3 + d3 + d4) & 1

    return (p1 | (d1<<1) | (p2<<2) | (d2<<3)
     | (p3<<4) | (d3<<5) | (p4<<6) | (d4<<7))


def header(magazine, packet):
    d1 = magazine | ((packet & 1) << 3)
    d2 = packet >> 1
    return [ham(d1), ham(d2)]


def page_header(number, text):
    text += (" " * (32 - len(text)))
    magazine = number / 100
    number %= 100
    return header(magazine, 0) + [
        ham(number % 10),
        ham(number / 10),
        ham(0), ham(0), ham(0), ham(0),
        ham(0), ham(0)
    ] + [parity(ord(x)) for x in text]


def translate(line):
    ret = ""
    escaped = False
    for c in line:
        if escaped:
            ret += chr(ord(c) - 0x40)
            escaped = False
        elif ord(c) == 0x1b:
            escaped = True
        elif ord(c) == 0x10:
            ret += 0x0d
        elif ord(c) > 0x80:
            ret += chr(ord(c) - 0x80)
        else:
            ret += c
    return ret


def gen_page_data(arg):
    sys.stderr.write(arg + '\n')
    ret = ""
    lines = map(translate, file(arg, 'rb+').read().split('\x0d\x0a'))

    for line in lines:
        if line.startswith('PN,'):
            page_number = int(line.split(',')[1]) / 100
            magazine = page_number / 100
            ret += ''.join(
                [chr(x) for x in page_header(page_number, "boomlinde")]
            )
        elif line.startswith('OL,'):
            _, row, data = line.split(',', 2)
            row = int(row)
            data = data[:40]
            ret += ''.join(
                [chr(x) for x in header(magazine, row)] +
                [chr(parity(ord(x))) for x in data]
            )

    return ret


if __name__ == '__main__':
    data = ""
    for arg in sys.argv[1:]:
        data += gen_page_data(arg)

    while True:
        sys.stdout.write(data)
